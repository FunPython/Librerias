# Librerias

Librerias creadas por la comunidad para optimizar distintas acciones de codigo necesarias para los diferentes proyectos.

# Librerias implementadas
# scf

Destinada para la fácil configuración de ssid y clave o contraseña via WiFi como Web Server para la conexión a red, a través del uso de un botón.

Funciones:
-confacces(pin), usaba para configurar la ssid y WiFi, en la que la variable pin es la definida por la cual se emitirá el dato digital al dispositivo. De manera que si el dato leido es igual a 0, se habilitará
el web server del dispositivo para la configuración de red, de lo contrario se conecta a la red antes establecida. Para entrar a la configuración es necesario conectarse al accespoint del dispositivo y entrar a su ip
local para visualizar en el browser los campos de configuración.


